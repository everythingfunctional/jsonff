module parse_json_test
    use erloff, only: error_list_t
    use integer_input_m, only: integer_input_t
    use iso_varying_string, only: VARYING_STRING, operator(//), put, var_str
    use jsonff, only: &
            fallible_json_value_t, &
            json_array_t, &
            json_element_t, &
            json_false_t, &
            json_integer_t, &
            json_null_t, &
            json_number_t, &
            json_object_t, &
            json_string_t, &
            json_true_t, &
            parse_json, &
            parse_json_from_file, &
            INVALID_INPUT
    use number_input_m, only: number_input_t
    use strff, only: NEWLINE
    use vegetables, only: &
            example_t, &
            input_t, &
            result_t, &
            test_item_t, &
            assert_equals, &
            assert_not, &
            assert_that, &
            describe, &
            fail, &
            it, &
            succeed

    implicit none
    private
    public :: test_parse_json

    character(len=*), parameter :: EXPANDED_EXAMPLE = &
   '{' // NEWLINE &
// '    "glossary" : {' // NEWLINE &
// '        "title" : "example glossary",' // NEWLINE &
// '        "GlossDiv" : {' // NEWLINE &
// '            "title" : "S",' // NEWLINE &
// '            "GlossList" : {' // NEWLINE &
// '                "GlossEntry" : {' // NEWLINE &
// '                    "ID" : 101,' // NEWLINE &
// '                    "SortAs" : "SGML",' // NEWLINE &
// '                    "GlossTerm" : "Standard Generalized Markup Language",' // NEWLINE &
// '                    "Acronym" : "SGML",' // NEWLINE &
// '                    "Abbrev" : "ISO 8879:1986",' // NEWLINE &
// '                    "GlossDef" : {' // NEWLINE &
// '                        "para" : "A meta-markup language, used to create markup languages such as DocBook.",' // NEWLINE &
// '                        "GlossSeeAlso" : [' // NEWLINE &
// '                            "GML",' // NEWLINE &
// '                            "XML"' // NEWLINE &
// '                        ]' // NEWLINE &
// '                    },' // NEWLINE &
// '                    "GlossSee" : 123.456' // NEWLINE &
// '                }' // NEWLINE &
// '            }' // NEWLINE &
// '        }' // NEWLINE &
// '    }' // NEWLINE &
// '}'
contains
    function test_parse_json() result(tests)
        type(test_item_t) :: tests

        tests = describe( &
                "parse_json", &
                [ it("parsing an empty string returns an error", check_parse_empty) &
                , it("can parse null", check_parse_null) &
                , it("can parse true", check_parse_true) &
                , it("can parse false", check_parse_false) &
                , it( &
                        "can parse a variety of numbers", &
                        [ example_t(number_input_t(var_str("0.0"), 0.0d0)) &
                        , example_t(number_input_t(var_str("-0.0"), 0.0d0)) &
                        , example_t(number_input_t(var_str("2.0"), 2.0d0)) &
                        , example_t(number_input_t(var_str("-2.0"), -2.0d0)) &
                        , example_t(number_input_t(var_str("0.2"), 0.2d0)) &
                        , example_t(number_input_t(var_str("1.2e3"), 1.2d3)) &
                        , example_t(number_input_t(var_str("1.2E+3"), 1.2d3)) &
                        , example_t(number_input_t(var_str("1.2e-3"), 1.2d-3)) &
                        , example_t(number_input_t(var_str("20e1"), 20.0d1)) &
                        ], &
                        check_parse_number) &
                , it( &
                        "when parsing a number, tracks how many digits of precision there were", &
                        check_number_significant_digits) &
                , it("can parse a variety of integers", &
                        [ example_t(integer_input_t(var_str("0"), 0)) &
                        , example_t(integer_input_t(var_str("-0"), 0)) &
                        , example_t(integer_input_t(var_str("3"), 3)) &
                        ], &
                        check_parse_integer) &
                , it("can parse a string", check_parse_string) &
                , it("can parse an empty array", check_parse_empty_array) &
                , it("can parse an array with a single element", check_parse_single_array) &
                , it("can parse an array with multiple elements", check_parse_multi_array) &
                , it("can parse an empty object", check_parse_empty_object) &
                , it("can parse an object with a single member", check_parse_single_object) &
                , it("can parse an object with multiple members", check_parse_multi_object) &
                , it("can parse complex data", check_parse_expanded_json) &
                , it("can parse data from a file", check_parse_from_file) &
                , it("fails if there is trailing content", check_trailing_content) &
                ])
    end function

    function check_parse_empty() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("")
        errors = json%errors()

        result_ = assert_that(errors%has_any(), errors%to_string())
    end function

    function check_parse_null() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(" null ")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_null_t)
                result_ = succeed("Got null")
            end select
        end if
    end function

    function check_parse_true() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(" true ")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_true_t)
                result_ = succeed("Got true")
            end select
        end if
    end function

    function check_parse_false() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(" false ")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_false_t)
                result_ = succeed("Got false")
            end select
        end if
    end function

    function check_parse_number(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        select type (input)
        type is (number_input_t)
            json = parse_json(input%string())
            errors = json%errors()
            result_ = assert_not(errors%has_any(), errors%to_string())
            if (result_%passed()) then
                select type (element => json%value_())
                type is (json_number_t)
                    result_ = assert_equals( &
                            input%value_(), &
                            element%get_value(), &
                            "Original string: " // input%string())
                class default
                    result_ = fail("expected a json_number_t, but got: " // element%to_compact_string())
                end select
            end if
        class default
            result_ = fail("Expected to get a number_input_t")
        end select
    end function

    function check_number_significant_digits() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("1.23e4")
        errors = json%errors()
        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_number_t)
                result_ = assert_equals(3, element%precision())
            class default
                result_ = fail("expected a json_number_t, but got: " // element%to_compact_string())
            end select
        end if
    end function

    function check_parse_integer(input) result(result_)
        class(input_t), intent(in) :: input
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        select type (input)
        type is (integer_input_t)
            json = parse_json(input%string())
            errors = json%errors()
            result_ = assert_not(errors%has_any(), errors%to_string())
            if (result_%passed()) then
                select type (element => json%value_())
                type is (json_integer_t)
                    result_ = assert_equals( &
                            input%value_(), &
                            element%get_value(), &
                            "Original string: " // input%string())
                class default
                    result_ = fail("expected a json_integer_t, but got: " // element%to_compact_string())
                end select
            end if
        class default
            result_ = fail("Expected to get a integer_input_t")
        end select
    end function

    function check_parse_string() result(result_)
        type(result_t) :: result_

        character(len=*), parameter :: THE_STRING = &
                '"AB\"\\\/\b\n\r\t\u1a2f"'
        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(THE_STRING)
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_string_t)
                result_ = assert_equals(THE_STRING, element%to_compact_string())
            end select
        end if
    end function

    function check_parse_empty_array() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("[ ]")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            select type (element => json%value_())
            type is (json_array_t)
                result_ = assert_equals(0, element%length())
            end select
        end if
    end function

    function check_parse_single_array() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("[20e1]")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals("[2.0e2]", json_%to_compact_string())
            end associate
        end if
    end function

    function check_parse_multi_array() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("[null, null,null]")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals("[null,null,null]", json_%to_compact_string())
            end associate
        end if
    end function

    function check_parse_empty_object() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("{ }")
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals("{}", json_%to_compact_string())
            end associate
        end if
    end function

    function check_parse_single_object() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json('{"first" : null}')
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals('{"first":null}', json_%to_compact_string())
            end associate
        end if
    end function

    function check_parse_multi_object() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json('{"first" : null, "second" : null, "third" : null}')
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals( &
                        '{"first":null,"second":null,"third":null}', &
                        json_%to_compact_string())
            end associate
        end if
    end function

    function check_parse_expanded_json() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json(EXPANDED_EXAMPLE)
        errors = json%errors()

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals(EXPANDED_EXAMPLE, json_%to_expanded_string())
            end associate
        end if
    end function

    function check_parse_from_file() result(result_)
        type(result_t) :: result_

        character(len=*), parameter :: TEMP_FILE_NAME = "temp_file.json"
        type(error_list_t) :: errors
        integer :: file_unit
        type(fallible_json_value_t) :: json

        open(newunit = file_unit, file = TEMP_FILE_NAME, action = "WRITE", status = "REPLACE")
        call put(file_unit, EXPANDED_EXAMPLE)
        close(file_unit)

        json = parse_json_from_file(TEMP_FILE_NAME)
        errors = json%errors()

        open(newunit = file_unit, file = TEMP_FILE_NAME)
        close(file_unit, status = "DELETE")

        result_ = assert_not(errors%has_any(), errors%to_string())
        if (result_%passed()) then
            associate(json_ => json%value_())
                result_ = assert_equals(EXPANDED_EXAMPLE, json_%to_expanded_string())
            end associate
        end if
    end function

    function check_trailing_content() result(result_)
        type(result_t) :: result_

        type(error_list_t) :: errors
        type(fallible_json_value_t) :: json

        json = parse_json("null # with trailing content")
        errors = json%errors()

        result_ = assert_that(errors.hasType.INVALID_INPUT, errors%to_string())
    end function
end module
