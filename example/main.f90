!> Simple reader for JSON files
program json_cat
    use, intrinsic :: iso_fortran_env, only : error_unit, output_unit
    use iso_varying_string, only : put_line
    use jsonff, only : parse_json_from_file, fallible_json_value_t, json_value_t
    use erloff, only : error_list_t
    implicit none
    class(json_value_t), allocatable :: json
    type(fallible_json_value_t) :: fallible_json
    type(error_list_t) :: errors
    character(len=:), allocatable :: argument
    integer :: iarg, narg

    narg = command_argument_count()
    if (narg == 0) then
        error stop "Please provide a JSON file as command line argument"
    end if

    do iarg = 1, narg
        call get_argument(iarg, argument)

        write(error_unit, '("#", *(1x, g0))') "Reading from", argument
        fallible_json = parse_json_from_file(argument)
        if (fallible_json%failed()) then
            errors = fallible_json%errors()
            call put_line(error_unit, errors%to_string())
            cycle
        end if

        json = fallible_json%value_()

        call put_line(output_unit, json%to_expanded_string())
    end do

contains

    !> Obtain the command line argument at a given index
    subroutine get_argument(idx, arg)
        !> Index of command line argument, range [0:command_argument_count()]
        integer, intent(in) :: idx
        !> Command line argument
        character(len=:), allocatable, intent(out) :: arg

        integer :: length, stat

        call get_command_argument(idx, length=length, status=stat)
        if (stat /= 0) then
            return
        endif

        allocate(character(len=length) :: arg, stat=stat)
        if (stat /= 0) then
            return
        endif

        if (length > 0) then
            call get_command_argument(idx, arg, status=stat)
            if (stat /= 0) then
                deallocate(arg)
                return
            end if
        end if

    end subroutine get_argument

end program json_cat
