module jsonff_json_number_m
    use iso_varying_string, only: varying_string
    use jsonff_json_value_m, only: json_value_t
    use strff, only: to_string

    implicit none
    private
    public :: json_number_t

    type, extends(json_value_t) :: json_number_t
        private
        double precision :: number
        integer :: precision_
        logical :: precision_provided_
    contains
        private
        procedure, public :: get_value
        procedure, public :: precision
        procedure, public :: to_compact_string => number_to_string
        procedure, public :: to_expanded_string => number_to_string
        procedure, public :: to_lines
    end type

    interface json_number_t
        module procedure constructor
    end interface
contains
    elemental function constructor(number, precision) result(json_number)
        double precision, intent(in) :: number
        integer, intent(in), optional :: precision
        type(json_number_t) :: json_number

        if (present(precision)) then
            json_number%precision_provided_ = .true.
            json_number%precision_ = precision
        else
            json_number%precision_provided_ = .false.
        end if
        json_number%number = number
    end function

    elemental function get_value(self) result(number)
        class(json_number_t), intent(in) :: self
        double precision :: number

        number = self%number
    end function

    elemental function precision(self)
        class(json_number_t), intent(in) :: self
        integer :: precision

        precision = self%precision_
    end function

    pure function number_to_string(self) result(string)
        class(json_number_t), intent(in) :: self
        type(varying_string) :: string

        if (self%precision_provided_) then
            string = to_string(self%number, self%precision_)
        else
            string = to_string(self%number)
        end if
    end function

    pure function to_lines(self) result(lines)
        class(json_number_t), intent(in) :: self
        type(varying_string), allocatable :: lines(:)

        lines = [self%to_compact_string()]
    end function
end module
